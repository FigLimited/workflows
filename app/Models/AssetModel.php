<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetModel extends Model
{
    protected $appends = ['label', 'value'];

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getLabelAttribute()
    {
        return "{$this->name}";
    }

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getValueAttribute()
    {
        return $this->id;
    }
}
