<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $appends = ['label', 'value'];

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getLabelAttribute()
    {
        return "{$this->name}";
    }

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getValueAttribute()
    {
        return $this->id;
    }

    /**
     * relationships
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function invoice_address()
    {
        return $this->belongsTo(Address::class, 'invoice_address_id');
    }

    public function delivery_address()
    {
        return $this->belongsTo(Address::class, 'delivery_address_id');
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class)->orderBy('name', 'ASC');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function sales_rep()
    {
        return $this->belongsTo(User::class);
    }
}
