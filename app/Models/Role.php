<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    protected $appends = ['label', 'value'];

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getLabelAttribute()
    {
        return "{$this->name}";
    }

    /**
     * label accessor for key/value listing of data in VueJS
     */
    public function getValueAttribute()
    {
        return $this->id;
    }

    /**
     * relationships
     */
    public function assetModels()
    {
        return $this->hasMany(AssetModel::class)->orderBy('name', 'ASC');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'permissions')->as('permissions');
    }

    public function processes()
    {
        return $this->belongsToMany(Process::class, 'entries')->as('entries');
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

}
