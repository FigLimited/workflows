<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Job extends Model
{
    protected $guarded = [];

    protected $appends = ['nett', 'gross', 'engineers_quote', 'due_date'];

    /**
     * sales quote & priority
     */
    public function getQuoteAttribute()
    {
        return ($this->sales_quote + $this->priority_amount) / 100;
    }

    /**
     * delivery & packaging
     */
    public function getExtraAttribute()
    {
        return ($this->delivery + $this->packaging) / 100;
    }

    /**
     * everything except the VAT
     */
    public function getNettAttribute()
    {
        return ($this->sales_quote + $this->priority_amount + $this->delivery + $this->packaging) / 100;
    }

    /**
     * VAT rate in %
     */
    public function getVatRateAttribute($value)
    {
        return $value / 100;  //  2000 => 20.00
    }

    /**
     * Extra VAT amount only
     */
    public function getExtraVatAttribute()
    {
        return $this->extra * ($this->vat_rate / 100);  //  2000 => 20.00
    }

    /**
     * VAT amount only
     */
    public function getVatAttribute()
    {
        return $this->nett * ($this->vat_rate / 100);  //  2000 => 20.00
    }

    /**
     * total payable
     */
    public function getGrossAttribute()
    {
        return $this->nett * (1 + ($this->vat_rate / 100));  //  2000 => 20.00
    }

    /**
     * based on approved date + lead days
     */
    public function getDueDateAttribute()
    {
        $date = new Carbon($this->approved_at);
        return $date->addDays($this->priority->lead_days)->toDateString();
    }


    public function getEngineersRateAttribute($value)
    {
        return $value / 100;  //  pence to pounds
    }

    public function getEngineersTimeAttribute($value)
    {
        return $value / 60;  //  pence to pounds
    }

    public function getEngineersQuoteAttribute()
    {
        return $this->engineers_rate * $this->engineers_time;
    }


    /**
     * relationships
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function engineer()
    {
        return $this->belongsTo(User::class, 'engineer_id');
    }

    public function asset_manufacturer()
    {
        return $this->belongsTo(AssetManufacturer::class);
    }

    public function asset_model()
    {
        return $this->belongsTo(AssetModel::class);
    }

    public function subcontractor()
    {
        return $this->belongsTo(Supplier::class, 'subcontractor_id')->where('subcontractor', true);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function note_sales()
    {
        $settings = \Setting::all();
        return $this->hasOne(Note::class)->where('note_type_id', 7);
    }

    public function note_internal()
    {
        $settings = \Setting::all();
        return $this->hasOne(Note::class)->where('note_type_id', 3);
    }

    public function note_engineer()
    {
        $settings = \Setting::all();
        return $this->hasOne(Note::class)->where('note_type_id', 6);
    }


    public static function dashboard($roles)
    {
        $notAllRoles = true;
        $rolesArray = $roles->map(function($item, $key) {
            return $item->permissions->role_id;
        });

        $r = Role::whereIn('id', $rolesArray)->where('see_all_jobs', 1)->get();
        if(count($r) > 0) {
            $notAllRoles = false;
        }

        $jobs = self::whereHas('status', function ($q) {
            return $q->where('dashboard', 1);
        })
        ->when($notAllRoles, function($z) use($rolesArray) {
            return $z->whereIn('role_id', $rolesArray);
        })
        ->with([
            'priority',
            'customer',
            'contact',
            'engineer',
            //'note_internal',
            //'note_sales',
            //'note_fault.note_type',
            //'note_fault_details.note_type',
            //'note_repairs.note_type',
            //'note_final_test.note_type',
            'asset_model',
            'asset_manufacturer',
            //'role', 'log_2', 'log_3', 'log_4', 'log_5', 'log_7', 'log_8',
            //'logs.status', 'logs.user',
            //'photographs',
            //'posts.user'
        ])
        ->orderBy('priority_id', 'DESC')
        ->limit(200)
        ->get();

        $statuses = [];
        foreach($jobs as $job) {
            $statuses[$job->status_id]['info'] = $job->status;
            $statuses[$job->status_id]['jobs'][] = $job;
        }

        $sort = [];
        foreach ($statuses as $key => $row)
        {
            $sort[$key] = $row['info']->sort;
        }
        array_multisort($sort, SORT_ASC, $statuses);

        return array_values($statuses);
    }

    /**
     * search method
     */
    public static function search($input)
    {
        \DB::enableQueryLog();
        $jobId             = $input['job_id'];
        $description       = (isset($input['description'])) ? $input['description'] : null;
        $salesRepId        = (isset($input['sales_rep_id'])) ? $input['sales_rep_id'] : null;
        $customer          = (isset($input['customer'])) ? $input['customer'] : null;
        $contact           = (isset($input['contact'])) ? $input['contact'] : null;
        $customerReference = (isset($input['customer_reference'])) ? $input['customer_reference'] : null;
        $assetManufacturer = (isset($input['asset_manufacturer'])) ? $input['asset_manufacturer'] : null;
        $assetModel        = (isset($input['asset_model'])) ? $input['asset_model'] : null;
        $assetSerial       = (isset($input['asset_serial'])) ? $input['asset_serial'] : null;
        $createdAtFrom     = (isset($input['created_at_from'])) ? $input['created_at_from'] : null;
        $createdAtTo       = (isset($input['created_at_to'])) ? $input['created_at_to'] : null;
        $statusId          = (isset($input['status_id'])) ? $input['status_id'] : null;
        $orderNumber       = (isset($input['order_number'])) ? $input['order_number'] : null;
        $warranty          = (isset($input['warranty'])) ? $input['warranty'] : null;

        error_log('CAF: '.$jobId);

        $jobs = Job::with('customer', 'contact', 'asset_manufacturer', 'asset_model', 'status')
        ->when($jobId, function($q) use($jobId) {
            $q->where('id', $jobId);
        })->when($description, function($q) use($description) {
            $q->where('description', 'LIKE', '%'.$description.'%');
        })->when($salesRepId, function($q) use($salesRepId) {
            $q->whereHas('customer', function($q) use($salesRepId) {
                $q->where('sales_rep_id', $salesRepId);
            });
        })->when($customer, function($q) use($customer) {
            $q->whereHas('customer', function($q) use($customer) {
                $q->where('name', 'LIKE', '%'.$customer.'%');
            });
        })->when($contact, function($q) use($contact) {
            $q->whereHas('contact', function($q) use($contact) {
                $q->where('name', 'LIKE', '%'.$contact.'%');
            });
        })->when($customerReference, function($q) use($customerReference) {
            $q->where('customer_reference', 'LIKE', '%'.$customerReference.'%');
        })->when($assetManufacturer, function($q) use($assetManufacturer) {
            $q->where(function($query) use($assetManufacturer) {
                $query->where('asset_manufacturer_name', 'LIKE', '%'.$assetManufacturer.'%')
                    ->orWhereHas('asset_manufacturer', function($q2) use($assetManufacturer) {
                        $q2->where('name', 'LIKE', '%'.$assetManufacturer.'%');
                });
            });
        })->when($assetModel, function($q) use($assetModel) {
            $q->where(function($query) use($assetModel) {
                $query->where('asset_model_name', 'LIKE', '%'.$assetModel.'%')
                    ->orWhereHas('asset_model', function($q2) use($assetModel) {
                        $q2->where('name', 'LIKE', '%'.$assetModel.'%');
                });
            });
        })->when($assetSerial, function($q) use($assetSerial) {
            $q->where('asset_serial', 'LIKE', '%'.$assetSerial.'%');
        })->when($createdAtFrom, function($q) use($createdAtFrom) {
            $q->whereDate('created_at', '>=', $createdAtFrom);
        })->when($createdAtTo, function($q) use($createdAtTo) {
            $q->whereDate('created_at', '<=', $createdAtTo);
        })->when($statusId, function($q) use($statusId) {
            $q->where('status_id', $statusId);
        })->when($orderNumber, function($q) use($orderNumber) {
            $q->where('order_number', 'LIKE', '%'.$orderNumber.'%');
        })->when($warranty, function($q) use($warranty) {
            $q->where('under_warranty', $warranty);
        })->orderBy('id', 'desc')->limit(100)->get();

        // error_log(json_encode(\DB::getQueryLog()));
        return $jobs;
    }

}
