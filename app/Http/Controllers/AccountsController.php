<?php

namespace App\Http\Controllers;

use App\Models\Process;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    public function sageExport(Request $request)
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('accounts.sage_export', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function sageExportReport()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('accounts.sage_export_report', [
            'newProcesses' => $newProcesses
        ]);
    }
}
