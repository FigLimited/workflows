<?php

namespace App\Http\Controllers\Api;

use App\Jobs\SendInvoice;
use App\Models\Job;
use App\Models\User;
use App\Mail\SalesInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
    public function sageExport(Request $request)
    {
        $invoicingStatus = 8;
        // $awaitingInvoiceStatus = \Setting::get('status_awaiting_invoice');

        $jobs = Job::where('sage_exported', 0)->where('status_id', $invoicingStatus)->whereNotNull('order_number')->with(['customer', 'contact', 'engineer'])->get();

        return response()->json([
            'jobs' => $jobs
        ]);
    }

    public function viewInvoice(Request $request, Job $job)
    {
        $user = User::where('api_token', $request->bearerToken())->first();
        $input = $request->all();

        $job->load('note_sales');

        $pdf = \PDF::loadView('accounts/pdf/invoice', ['job' => $job, 'user' => $user])->setPaper('a4', 'portrait');
        $output = $pdf->output();
        $path = str_random(12);
        file_put_contents($path.'.pdf', $output);

        return response()->json([
            'path' => '/'.$path.'.pdf'
        ]);
    }

    public function sendInvoice(Request $request, Job $job)
    {
        $user = User::where('api_token', $request->bearerToken())->first();
        $input = $request->all();

        $job->load('note_sales');

        $pdf = \PDF::loadView('accounts/pdf/invoice', ['job' => $job, 'user' => $user])->setPaper('a4', 'portrait');
        $output = $pdf->output();

        $to = $input['email'];
        Mail::to($to)->send(new SalesInvoice($job, $output));

        return response()->json([
            'input' => $request->all()
        ], 200);

    }

    public function export(Request $request)
    {
        $input = $request->all();

        $jobs = Job::whereIn('id', $input['jobs'])->with(['customer', 'note_sales'])->get();

        $head = ['Type', 'Account Reference', 'Reference', 'Date', 'Nominal A/C Ref', 'Details', 'Department Code', 'Net Amount', 'Tax Code', 'Tax Amount'];

        $cells = [];
        foreach ($jobs as $job):
            $nett = $job->nett;
            $xtra = $job->delivery + $job->packaging;
            $cell1 = [];
            $cell1[] = 'SI';
            $cell1[] = $job->customer->code;
            $cell1[] = 'Inv'.$job->id;
            $cell1[] = date('d/m/Y');
            $cell1[] = $job->role->nominal_code;
            $cell1[] = $job->note_sales->note;
            $cell1[] = $job->engineer->id;
            $cell1[] = $nett;
            $cell1[] = '11';
            $cell1[] = $job->vat;
            // echo implode(',', $cell)."\n";
            $cells[] = $cell1;

            //  check if carriage or p&p
            if($xtra > 0) {
                $cell2 = [];
                $cell2[] = 'SI';
                $cell2[] = $job->customer->code;
                $cell2[] = 'Inv'.$job->id;
                $cell2[] = date('d/m/Y');
                $cell2[] = '4905';
                $cell2[] = 'Carriage / Packaging';
                $cell2[] = $job->engineer->id;
                $cell2[] = $xtra;
                $cell2[] = '11';
                $cell2[] = $this->extraVat;
                // echo implode(',', $cell)."\n";
                $cells[] = $cell2;
            }

        endforeach;

        $path = (string)\Uuid::generate(4);
        $file = fopen('temp/'.$path.'.csv', 'w');
        fputcsv($file, $head);
        foreach($cells as $cell) {
            fputcsv($file, $cell);
        }

        return response()->json([
            'path' => '/temp/'.$path.'.csv'
        ]);
    }

    /**
     * for Sage Export Report
     */
    public function jobsDelivered(Request $request)
    {
        $input = $request->all();

        $jobs = Job::with('customer')->whereBetween('delivered_at', [$input['dates']['from'], $input['dates']['to']])->get();

        return response()->json([
            'input' => $input,
            'jobs' => $jobs
        ]);
    }
}
