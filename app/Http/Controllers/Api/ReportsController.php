<?php

namespace App\Http\Controllers\Api;

use App\Models\Job;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function outstandingOrderNumbers(Request $request, $pdf = false)
    {
        // $invoicingStatus = \Setting::get('status_awaiting_invoice');
        $invoicingStatus = 8;
        //  assumes all blank order numbers are null
        $salesReps = Job::where('status_id', $invoicingStatus)->whereNull('order_number')->with('customer.sales_rep')->get()->mapToGroups(function($val, $key) {
            return ($val->customer->sales_rep !== null) ? [$val->customer->sales_rep->full_name => $val] : ['Unassigned' => $val];
        });

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = User::where('api_token', $request->bearerToken())->first();
            $pdf = \PDF::loadView('reports/pdf/outstanding_order_numbers', ['salesReps' => $salesReps, 'user' => $user])->setPaper('a4', 'portrait');
            // return $pdf->stream('outstanding-order-numbers.pdf', ["Attachment" => false]);
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'salesReps' => $salesReps
        ]);
    }

    public function awaitingAcceptance(Request $request, $pdf = false, $mine = false)
    {
        // $awaitingStatus = \Setting::get('status_awaiting_acceptance');
        $awaitingStatus = 4;

        $salesReps = Job::where('status_id', $awaitingStatus)->with('customer.sales_rep')->when($mine, function($q) use($mine) {
            $q->whereHas('customer', function($q2) use($mine) {
                $q2->where('sales_rep_id', $mine);
            });
        })->get()->mapToGroups(function($val, $key) {
            return ($val->customer->sales_rep !== null) ? [$val->customer->sales_rep->full_name => $val] : ['Unassigned' => $val];
        });

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = User::where('api_token', $request->bearerToken())->first();
            $pdf = \PDF::loadView('reports/pdf/awaiting_acceptance', ['salesReps' => $salesReps, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'salesReps' => $salesReps,
            'junk' => $awaitingStatus
        ]);
    }

    public function byEngineer(Request $request, $pdf = false)
    {
        // $inProgressStatus = \Setting::get('status_in_progress');
        $inProgressStatus = 6;

        $engineers = Job::where('status_id', $inProgressStatus)->with('customer', 'engineer')->get()->mapToGroups(function($val, $key) {
            return ($val->engineer !== null) ? [$val->engineer->full_name => $val] : ['Unassigned' => $val];
        });

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = User::where('api_token', $request->bearerToken())->first();
            $pdf = \PDF::loadView('reports/pdf/by_engineer', ['engineers' => $engineers, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'engineers' => $engineers,
            'newProcesses' => $newProcesses
        ]);
    }

    public function awaitingCollection(Request $request, $pdf = false)
    {
        // $collectionStatus = \Setting::get('status_for_collection');
        $collectionStatus = 1;

        $jobs = Job::where('status_id', $collectionStatus)->with('customer')->get();

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = \Auth::user();
            $pdf = \PDF::loadView('reports/pdf/awaiting_collection', ['jobs' => $jobs, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'jobs' => $jobs,
            'newProcesses' => $newProcesses
        ]);
    }


    public function awaitingDelivery(Request $request, $pdf = false)
    {
        // $collectionStatus = \Setting::get('status_for_collection');
        $deliveryStatus = 7;

        $jobs = Job::where('status_id', $deliveryStatus)->with('customer')->get();

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = \Auth::user();
            $pdf = \PDF::loadView('reports/pdf/awaiting_delivery', ['jobs' => $jobs, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'jobs' => $jobs,
            'newProcesses' => $newProcesses
        ]);
    }

    public function jobsOverdue(Request $request, $pdf = false)
    {
        // $goAheadStatus = [\Setting::get('status_on_hold'), \Setting::get('status_in_progress')];
        $goAheadStatus = [5, 6];

        $today = \Carbon\Carbon::now();

        $jobs = Job::whereNotNull('approved_at')->whereIn('status_id', $goAheadStatus)->with('customer', 'priority')->get()->filter(function($val) use($today) {

            //  get approval date TODO:
            $appDate = new \Carbon\Carbon($val->approved_at);

            //  get todays date TODO:
            $newToday = clone($today);
            $newToday->subDays($val->priority->lead_days);

            Log::info($val->id.': '.$appDate->format('Y-m-d').' - '.$newToday->format('Y-m-d').' = '.$appDate->lessThan($newToday));

            return $appDate <= $newToday;
        });

        if($pdf) {
            $user = \Auth::user();
            $pdf = \PDF::loadView('reports/pdf/jobs_overdue', ['jobs' => $jobs, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        $newProcesses = Process::whereNull('from_status_id')->get();

        return response()->json([
            'jobs' => $jobs,
            'newProcesses' => $newProcesses
        ]);

    }

    public function monthlyOutput(Request $request, $pdf = false, $month = false)
    {
        if($month !== false && $month != 0) {
            $dateStart = \Carbon\Carbon::createFromFormat('Y-m-d', $month)->startOfMonth();
            $dateEnd   = \Carbon\Carbon::createFromFormat('Y-m-d', $month)->endOfMonth();
        } else {
            $dateStart = \Carbon\Carbon::now()->startOfMonth();
            $dateEnd   = \Carbon\Carbon::now()->endOfMonth();
        }

        $engineers = Job::whereBetween('completed_at', [$dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d')])->whereNotNull('started_at')->with('customer', 'engineer')->get()->map(function($val, $key) {
            $val['turnaround'] =
                \Carbon\Carbon::createFromFormat('Y-m-d', $val['completed_at'])
                    ->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d', $val['started_at']));
            return $val;
        })->mapToGroups(function($val, $key) {
            return ($val->engineer !== null) ? [$val->engineer->full_name => $val] : ['Unassigned' => $val];
        });
        // dd($engineers['Unassigned'][0]);

        $newProcesses = Process::whereNull('from_status_id')->get();

        if($pdf) {
            $user = \Auth::user();
            $pdf = \PDF::loadView('reports/pdf/monthly_output', ['engineers' => $engineers, 'user' => $user])->setPaper('a4', 'portrait');
            $output = $pdf->output();
            $path = str_random(12);
            file_put_contents($path.'.pdf', $output);

            return response()->json([
                'path' => '/'.$path.'.pdf'
            ]);
        }

        return response()->json([
            'engineers' => $engineers,
            'month' => $month,
            'newProcesses' => $newProcesses
        ]);
    }
}
