<?php

namespace App\Http\Controllers\Api;

use App\Models\Job;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobsController extends Controller
{
    public function show(Job $job)
    {
        $job->load('priority', 'logs.status', 'customer', 'contact', 'asset_manufacturer', 'asset_model', 'note_internal', 'note_engineer', 'status', 'subcontractor');

        return response()->json([
            'job' => $job
        ]);
    }

    public function dashboard(Request $request)
    {
        $user = User::where('api_token', $request->bearerToken())->with('roles')->first();

        return response()->json([
            'message' => 'success',
            'statuses' => Job::dashboard($user->roles),
            // 'roles' => $roles
            // 'user' => $user
            // 'request' => $request->bearerToken()
        ]);
    }

    /**
     * Search method
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function search(Request $request)
    {
        $input = $request->all();
        $jobs = Job::search($input);

        return response()->json([
            'jobs' => $jobs,
            'request' => $input
        ]);
    }
}
