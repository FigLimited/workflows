<?php

namespace App\Http\Controllers\Api;

use App\Models\Job;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProcessesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $process)
    {
        return response()->json([
            'process' => $process
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function edit(Process $process)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Process $process)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $process)
    {
        //
    }

    public function new()
    {
        return response()->json([
            'newProcesses' => Process::whereNull('from_status_id')->get()
        ]);
    }

    /**
     * create a job
     */
    public function postNew(Request $request, Process $process)
    {
        $input = $request->all();

        $user = User::where('api_token', $request->bearerToken())->first();

        $input['job']['status_id'] = $process->to_status_id;
        $job = Job::create($input['job']);

        foreach($input['notes'] as $note) {
            $job->notes()->create($note);
        }

        $input['log']['status_id'] = $process->to_status_id;
        $input['log']['user_id'] = $user->id;
        $job->logs()->create($input['log']);

        return response()->json([
            'job' => $job
        ]);
    }
}
