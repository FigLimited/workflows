<?php

namespace App\Http\Controllers\Api;

use App\Models\AssetManufacturer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssetManufacturersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'assetManufacturers' => AssetManufacturer::orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetManufacturer  $assetManufacturer
     * @return \Illuminate\Http\Response
     */
    public function show(AssetManufacturer $assetManufacturer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetManufacturer  $assetManufacturer
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetManufacturer $assetManufacturer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetManufacturer  $assetManufacturer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetManufacturer $assetManufacturer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetManufacturer  $assetManufacturer
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetManufacturer $assetManufacturer)
    {
        //
    }

    /**
     * get all contacts for a customer
     */
    public function assetModels(AssetManufacturer $assetManufacturer)
    {
        return response()->json([
            'assetModels' => $assetManufacturer->assetModels
        ]);
    }
}
