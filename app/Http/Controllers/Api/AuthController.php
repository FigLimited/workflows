<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|between:6,25'
        ]);

        $user = User::where('username', $request->username)->first();

        if($user && \Hash::check($request->password, $user->password)) {
            $user->load('roles');
            return response()->json([
                'user' => $user
            ], 200);
        }

        return response()->json([
            'test' => \Hash::check($request->password, $user->password)
        ], 422);
    }

    public function getDevEmail()
    {
        return response()->json([
            'email' => config('mail.dev_email'),
            'email_dev' => config('mail.dev')
        ]);
    }
}
