<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    /**
     * show method
     *
     */
    public function show(Job $job)
    {
        return view('jobs.show', [
            'id' => $job->id
        ]);
    }

    /**
     * dashboard method
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function dashboard(Request $request)
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('jobs.dashboard', [
            'newProcesses' => $newProcesses
        ]);
    }

    /**
     * search method
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function search(Request $request)
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('jobs.search', [
            'newProcesses' => $newProcesses
        ]);
    }
}
