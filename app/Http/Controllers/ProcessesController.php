<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Process;
use Illuminate\Http\Request;

class ProcessesController extends Controller
{
    public function new(Request $request, Process $process)
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('processes.process_'.$process->id, [
            'processId' =>$process->id,
            'newProcesses' => $newProcesses
        ]);
        // $newProcesses = Process::whereNull('from_status_id')->get();

        // $customers = Customer::orderBy('name', 'ASC')->get();
        // $priorities = Priority::orderBy('id', 'ASC')->get();
        // $assetManufacturers = AssetManufacturer::orderBy('name', 'ASC')->get();
        // $departments = Role::where('job_type', 1)->get();

        // $priorities = Priority::where('active', 1)->orderBy('id', 'ASC')->get();
        // $subcontractors = Supplier::where('subcontractor', 1)->orderBy('name', 'ASC')->get();

        // return view('processes/process_'.$process->id, [
        //     'process' => $process,
        //     'customers' => $customers,
        //     'priorities' => $priorities,
        //     'assetManufacturers' => $assetManufacturers,
        //     'departments' => $departments,
        //     'priorities' => $priorities,
        //     'subcontractors' => $subcontractors,
        //     'newProcesses' => $newProcesses
        // ]);
    }
}
