<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function outstandingOrderNumbers()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.outstanding_order_numbers', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function awaitingAcceptance()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.awaiting_acceptance', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function byEngineer()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.by_engineer', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function awaitingCollection()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.awaiting_collection', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function awaitingDelivery()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.awaiting_delivery', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function jobsOverdue()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.jobs_overdue', [
            'newProcesses' => $newProcesses
        ]);
    }

    public function monthlyOutput()
    {
        $newProcesses = Process::whereNull('from_status_id')->get();

        return view('reports.monthly_output', [
            'newProcesses' => $newProcesses
        ]);
    }
}
