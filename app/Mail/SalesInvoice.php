<?php

namespace App\Mail;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SalesInvoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $job;

    protected $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job, $pdf)
    {
        $this->job = $job;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('accounts@neutronictechnologies.com')
            ->attachData($this->pdf, 'invoice.pdf')
            ->subject('Sales Invoice: N'.$job->id)
            ->view('jobs.emails.sales_invoice')->with([
                'job' => $this->job
            ]);
    }
}
