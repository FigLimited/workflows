<?php

use Illuminate\Database\Seeder;
use App\Models\Contact;
use App\Models\Customer;
use App\Models\Job;
use App\Models\Menu;
use App\Models\NoteType;
use App\Models\Priority;
use App\Models\Process;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;

class LiveDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Note Types
        $noteTypeFault        = NoteType::create(['name' => 'Fault']);
        $noteTypeFaultDetails = NoteType::create(['name' => 'Fault Details']);
        $noteTypeInternal     = NoteType::create(['name' => 'Internal Note']);
        $noteTypeRepairs      = NoteType::create(['name' => 'Repairs']);
        $noteTypeFinalTest    = NoteType::create(['name' => 'Final Test']);
        $noteTypeEngineer     = NoteType::create(['name' => 'Engineers Note']);
        $noteTypeSales        = NoteType::create(['name' => 'Sales Note']);

        \Setting::set('note_type_fault', $noteTypeFault->id);
        \Setting::set('note_type_fault_details', $noteTypeFaultDetails->id);
        \Setting::set('note_type_repairs', $noteTypeRepairs->id);
        \Setting::set('note_type_final_test', $noteTypeFinalTest->id);
        \Setting::set('note_type_internal', $noteTypeInternal->id);
        \Setting::set('note_type_engineer', $noteTypeEngineer->id);
        \Setting::set('note_type_sales', $noteTypeSales->id);

        //  Priorities
        Priority::create(['name' => 'Low Priority', 'class' => 'green darken-2', 'lead_days' => 30, 'markup' => 0, 'hex' => '388E3C']);
        Priority::create(['name' => 'Normal Service', 'class' => 'green darken-2', 'lead_days' => 10, 'markup' => 0, 'hex' => '388E3C']);
        Priority::create(['name' => 'High Priority', 'class' => 'orange darken-2', 'lead_days' => 3, 'markup' => 0, 'hex' => 'F57C00']);
        Priority::create(['name' => 'Emergency Service', 'class' => 'red darken-2', 'lead_days' => 1, 'markup' => 25, 'hex' => 'D32F2F']);

        //  Roles
        $guru       = Role::create(['name' => 'guru', 'see_all_jobs' => 1, 'is_guru' => 1]);
        $admin      = Role::create(['name' => 'admin', 'see_all_jobs' => 1, 'is_admin' => 1]);
        $engineer   = Role::create(['name' => 'engineer']);
        $mechanical = Role::create(['name' => 'mechanical', 'job_type' => 1]);
        $supply     = Role::create(['name' => 'supply sales', 'job_type' => 1, 'see_all_jobs' => 1]);
        $electronic = Role::create(['name' => 'electronic', 'job_type' => 1]);
        $condition  = Role::create(['name' => 'condition monitoring', 'job_type' => 1]);
        $logistics  = Role::create(['name' => 'logistics', 'job_type' => 1, 'see_all_jobs' => 1]);
        $servo      = Role::create(['name' => 'servo', 'job_type' => 1]);
        $driver     = Role::create(['name' => 'driver']);
        $accounts   = Role::create(['name' => 'accounts']);
        $sales      = Role::create(['name' => 'sales']);
        $public     = Role::create(['name' => 'public']);

        \Setting::set('role_guru', $guru->id);
        \Setting::set('role_admin', $admin->id);
        \Setting::set('role_engineer', $engineer->id);
        \Setting::set('role_mechanical', $mechanical->id);
        \Setting::set('role_electronic', $electronic->id);
        \Setting::set('role_driver', $driver->id);
        \Setting::set('role_accounts', $accounts->id);
        \Setting::set('role_sales', $sales->id);
        \Setting::set('role_public', $public->id);

        //  Statuses
        $forCollection      = Status::create(['name' => 'For Collection', 'active' => true, 'dashboard' => true]);  //  to Collect
        $engineeringQuote   = Status::create(['name' => 'Engineering Quote', 'active' => true, 'dashboard' => true]); //  to Quote (Eng)
        $salesQuote         = Status::create(['name' => 'Sales Quote', 'active' => true, 'dashboard' => true]);  //  to Quote (Sales)
        $awaitingAcceptance = Status::create(['name' => 'Awaiting Acceptance', 'active' => true, 'dashboard' => true]);  //  customer to approve
        $awaitingWork       = Status::create(['name' => 'On Hold', 'active' => true, 'dashboard' => true]);  //  goes to engineer paused
        $startWork          = Status::create(['name' => 'In Progress', 'active' => true, 'dashboard' => true]);  //  goes to go ahead
        $awaitingDelivery   = Status::create(['name' => 'For Delivery', 'active' => true, 'dashboard' => true]);  //  waiting to be delivered
        $awaitingInvoice    = Status::create(['name' => 'For Invoicing', 'active' => true, 'dashboard' => true]);  //  waiting to be invoiced
        $awaitingReceipt    = Status::create(['name' => 'Awaiting Payment', 'active' => false, 'dashboard' => false]);  //  waiting to be paid
        $jobComplete        = Status::create(['name' => 'Completed', 'active' => true, 'dashboard' => false]);  //  completed. Will move to archive after X days
        $archive            = Status::create(['name' => 'Archive', 'active' => true, 'dashboard' => false]);  //  history!
        \Setting::set('status_for_collection', $forCollection->id);
        \Setting::set('status_awaiting_acceptance', $awaitingAcceptance->id);
        \Setting::set('status_on_hold', $awaitingWork->id);
        \Setting::set('status_in_progress', $startWork->id);
        \Setting::set('status_awaiting_delivery', $awaitingDelivery->id);
        \Setting::set('status_awaiting_invoice', $awaitingInvoice->id);
        \Setting::set('status_archive', $archive->id);
        //  TODO
        \Setting::save();

        $settings = Setting::all();

        //  Processes
        $book                  = Process::create(['name' => 'Book a Collection', 'description' => '', 'from_status_id' => null, 'to_status_id' => $forCollection->id]);
        $book->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $create                = Process::create(['name' => 'Create a Job', 'description' => '', 'from_status_id' => null, 'to_status_id' => $engineeringQuote->id]);
        $create->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $receiveBookedjob      = Process::create(['name' => 'Receive Booked Job', 'description' => '', 'from_status_id' => $forCollection->id, 'to_status_id' => $engineeringQuote->id]);
        $receiveBookedjob->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $quoteByEngineer       = Process::create(['name' => 'Engineer Quote', 'description' => '', 'from_status_id' => $engineeringQuote->id, 'to_status_id' => $salesQuote->id]);
        $quoteByEngineer->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $quoteBySales          = Process::create(['name' => 'Sales Quote', 'description' => '', 'from_status_id' => $salesQuote->id, 'to_status_id' => $awaitingAcceptance->id]);
        $quoteBySales->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $customerApprovalQueue = Process::create(['name' => 'Queue Job', 'description' => '', 'from_status_id' => $awaitingAcceptance->id, 'to_status_id' => $awaitingWork->id]);
        $customerApprovalQueue->roles()->attach([$sales->id, $admin->id, $guru->id]);

        $customerApprovalStart = Process::create(['name' => 'Go Ahead', 'description' => '', 'from_status_id' => $awaitingAcceptance->id, 'to_status_id' => $startWork->id]);
        $customerApprovalStart->roles()->attach([$sales->id, $admin->id, $guru->id]);

        $customerDeclined      = Process::create(['name' => 'Customer Declined', 'description' => '', 'from_status_id' => $awaitingAcceptance->id, 'to_status_id' => $jobComplete->id]);
        $customerDeclined->roles()->attach([$sales->id, $admin->id, $guru->id]);

        $pauseJob              = Process::create(['name' => 'Pause Job', 'description' => 'Change to On Hold.', 'from_status_id' => $startWork->id, 'to_status_id' => $awaitingWork->id]);
        $pauseJob->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $resumeJob             = Process::create(['name' => 'Resume Job', 'description' => 'Change to In Progress. Any other In Progress Jobs by you will be changed to On Hold', 'from_status_id' => $awaitingWork->id, 'to_status_id' => $startWork->id]);
        $resumeJob->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $finishJob             = Process::create(['name' => 'Finish Job', 'description' => '', 'from_status_id' => $startWork->id, 'to_status_id' => $awaitingDelivery->id]);
        $finishJob->roles()->attach([$engineer->id, $admin->id, $guru->id]);

        $deliverJob            = Process::create(['name' => 'Deliver Job', 'description' => '', 'from_status_id' => $awaitingDelivery->id, 'to_status_id' => $awaitingInvoice->id]);
        $deliverJob->roles()->attach([$driver->id, $admin->id, $guru->id]);

        $invoiceJob            = Process::create(['name' => 'Invoice Job', 'description' => '', 'from_status_id' => $awaitingInvoice->id, 'to_status_id' => $awaitingReceipt->id]);
        $invoiceJob->roles()->attach([$accounts->id, $admin->id, $guru->id]);

        $receiveReceipt        = Process::create(['name' => 'Pay Job', 'description' => '', 'from_status_id' => $awaitingReceipt->id, 'to_status_id' => $jobComplete->id]);
        $receiveReceipt->roles()->attach([$accounts->id, $admin->id, $guru->id]);

        $archiveJob            = Process::create(['name' => 'Archive Job', 'description' => '', 'from_status_id' => $jobComplete->id, 'to_status_id' => $archive->id]);
        $archiveJob->roles()->attach([$accounts->id, $admin->id, $guru->id]);


        //  Menus
        // $menuMain      = Menu::create(['name' => 'Main Menu', 'role_id' => $guru->id]);

        // $menuHome      = Menu::create(['name' => 'Home', 'slug' => '/', 'role_id' => $guru->id], $menuMain);

        // $menuLogout    = Menu::create(['name' => 'Logout', 'slug' => '/logout', 'role_id' => $guru->id], $menuMain);

        // $menuRegister  = Menu::create(['name' => 'Register', 'slug' => '/register', 'role_id' => $guru->id], $menuMain);

        // $menuLogin     = Menu::create(['name' => 'Login', 'slug' => '/login', 'role_id' => $guru->id], $menuMain);

        // $menuJobs      = Menu::create(['name' => 'Jobs', 'role_id' => $guru->id]);

        // $menuDashboard = Menu::create(['name' => 'Dashboard', 'slug' => '/jobs/dashboard', 'role_id' => $guru->id], $menuJobs);

        // $menuSearch = Menu::create(['name' => 'Search', 'slug' => '/jobs/search', 'role_id' => $guru->id], $menuJobs);

        // $menuDeliveries = Menu::create(['name' => 'Deliveries', 'slug' => '/jobs/deliveries', 'role_id' => $guru->id], $menuJobs);

        // $menuReports = Menu::create(['name' => 'Reports', 'role_id' => $guru->id]);

        // $menuOutstandingOrderNumbers = Menu::create(['name' => 'Awaiting POs', 'slug' => '/reports/outstanding-order-numbers', 'role_id' => $guru->id], $menuReports);

        // $menuAwaitingAcceptance = Menu::create(['name' => 'Awaiting Acceptance', 'slug' => '/reports/awaiting-acceptance', 'role_id' => $guru->id], $menuReports);

        // $menuByEngineer = Menu::create(['name' => 'By Engineer', 'slug' => '/reports/by-engineer', 'role_id' => $guru->id], $menuReports);

        // $menuAwaitingCollection = Menu::create(['name' => 'Awaiting Collection', 'slug' => '/reports/awaiting-collection', 'role_id' => $guru->id], $menuReports);

        // $menuAwaitingDelivery = Menu::create(['name' => 'Awaiting Delivery', 'slug' => '/reports/awaiting-delivery', 'role_id' => $guru->id], $menuReports);

        // $menuOverdue = Menu::create(['name' => 'Jobs Overdue', 'slug' => '/reports/overdue', 'role_id' => $guru->id], $menuReports);

        // $menuMonthlyOutput = Menu::create(['name' => 'Monthly Output', 'slug' => '/reports/monthly-output', 'role_id' => $guru->id], $menuReports);

        // $menuAwaitingParts = Menu::create(['name' => 'Awaiting Parts', 'slug' => '/reports/awaiting-parts', 'role_id' => $guru->id], $menuReports);

        // $menuAccounts = Menu::create(['name' => 'Accounts', 'role_id' => $guru->id]);

        // $menuSageExport = Menu::create(['name' => 'Sage Export', 'slug' => '/accounts/sage-export', 'role_id' => $guru->id], $menuAccounts);

        // $menuSageExportReport = Menu::create(['name' => 'Sage Export Report', 'slug' => '/accounts/sage-export-report', 'role_id' => $guru->id], $menuAccounts);

        // $menuManagement = Menu::create(['name' => 'Management', 'role_id' => $guru->id]);

        // $menuManagementDashboard = Menu::create(['name' => 'Dashboard', 'slug' => '/management/dashboard', 'role_id' => $guru->id], $menuManagement);

        //  Customer
        Customer::create([
            'id' => 1,
            'name' => 'A Customer'
        ]);

        //  Contact
        Contact::create([
            'id' => 1,
            'customer_id' => 1,
            'name' => 'A Contact'
        ]);

        //  Jobs
        for($x = 100000; $x <= 100099; $x++) {
            Job::create([
                'id' => $x,
                'status_id' => rand(1, 8),
                'customer_id' => 1,
                'contact_id' => 1
            ]);
        }

        //  Settings
        \Setting::set('setting_eng_hourly_rate', 60);
        \Setting::set('setting_eng_parts_markup', 33);

        \Setting::save();
    }
}
