<?php

use Illuminate\Database\Seeder;
use App\Models\AssetManufacturer;
use App\Models\AssetModel;

class PostCopy extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = \App\Models\User::create([
            'id' => 1,
            'username' => 'admin',
            'password' => bcrypt(config('app.admin_password')),
            'api_token' => (string)\Uuid::generate(4)
        ]);
        // $user1->password = bcrypt(config('app.guru_password'));
        // $user1->api_token = (string)\Uuid::generate(4);
        // $user1->save();
        $user1->roles()->sync([1]);

        $user2 = \App\Models\User::create([
            'id' => 2,
            'username' => 'manager',
            'password' => bcrypt(config('app.admin_password')),
            'api_token' => (string)\Uuid::generate(4)
        ]);
        // $user1->password = bcrypt(config('app.guru_password'));
        // $user1->api_token = (string)\Uuid::generate(4);
        // $user1->save();
        $user2->roles()->sync([1]);

        // $user2 = \App\Models\User::find(10);
        // $user2->password = bcrypt(config('app.admin_password'));
        // $user1->api_token = (string)\Uuid::generate(4);
        // $user2->save();
        // $user2->roles()->sync([1]);

    }
}
