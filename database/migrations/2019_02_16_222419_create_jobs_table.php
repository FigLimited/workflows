<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            
            //  customer instructions/observations
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('contact_id')->unsigned()->nullable();
            $table->string('customer_reference', 100)->nullable();
            $table->string('order_number', 100)->nullable();
            $table->string('description')->nullable();

            $table->string('job_reference')->nullable();
            $table->string('bin')->nullable();

            $table->integer('status_id')->unsigned();

            $table->integer('priority_id')->unsigned()->default(1);
            //  priority markup
            $table->integer('priority_markup')->nullable();  //  percent
            $table->integer('priority_amount')->nullable();  //  amount

            $table->integer('asset_manufacturer_id')->unsigned()->nullable();
            $table->integer('asset_model_id')->unsigned()->nullable();
            $table->string('asset_serial')->nullable();

            //  backwards compatible
            $table->string('asset_manufacturer_name')->nullable();
            $table->string('asset_model_name')->nullable();

            $table->integer('role_id')->unsigned()->nullable();
            $table->integer('engineer_id')->unsigned()->nullable();

            //  engineers quote
            $table->integer('engineers_time')->unsigned()->nullable(); //  in minutes
            $table->integer('engineers_rate')->unsigned()->nullable(); //  in pence
            $table->integer('parts_markup')->unsigned()->nullable(); //  in percent

            //  sales quote
            $table->integer('sales_quote')->unaigned()->nullable();
            $table->integer('delivery')->unsigned()->nullable();
            $table->integer('packaging')->unsigned()->nullable();
            $table->integer('lead_days')->unsigned()->nullable();

            //  vat
            $table->integer('vat_rate')->unsigned()->default(2000);

            $table->boolean('rnr')->default(0);
            $table->boolean('ber')->default(0);

            //  signature
            $table->text('signature_svg')->nullable();
            $table->string('signature_print')->nullable();
            
            $table->boolean('under_warranty')->default(0);
            $table->boolean('sage_exported')->default(0);

            $table->integer('subcontractor_id')->unsigned()->nullable();

            //  permanent dates
            $table->date('costed_at')->nullable();
            $table->date('quoted_at')->nullable();
            $table->date('approved_at')->nullable();
            $table->date('started_at')->nullable();
            $table->date('completed_at')->nullable();
            $table->date('delivered_at')->nullable();
            
            $table->timestamps();

            //  indices
            $table->index(['customer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
