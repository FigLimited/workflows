import axios from 'axios'

export function login(payload) {
  return axios({
    method: 'POST',
    url: '/api/login',
    data: payload
  })
}

export function get(url) {
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
    }
  })
}

export function post(url, data) {
    return axios({
      method: 'POST',
      url: url,
      data: data,
      headers: {
        'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
      }
    })
  }

export function dashboard(url) {
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
    }
  })
}

export function jobsShow(url) {
  return axios({
    method: 'GET',
    url: url,
    headers: {
      'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
    }
  })
}

export function getProcess(processId) {
    return axios({
        method: 'GET',
        url: '/api/processes/' + processId,
        headers: {
            'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
        }
    })
}

export function postJobProcess(processId, data) {
    return axios({
        method: 'POST',
        url: '/api/processes/' + processId + '/process',
        data: data,
        headers: {
            'Authorization': `Bearer ${Vue.localStorage.get('api_token')}`
        }
    })
}
