
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

var numeral = require("numeral")
Vue.filter("formatJobNumber", function (value) {
  return numeral(value).format("000000")
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components', false, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

// Vue.component('login-component', require('./components/LoginComponent.vue'));
// Vue.component('jobs-dashboard-component', require('./components/JobsDashboardComponent.vue'));

import VModal from 'vue-js-modal'
Vue.use(VModal)

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)

Vue.use(require('vue-moment'));

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {});

    var elem = document.querySelector('.collapsible.expandable');
    var instance = M.Collapsible.init(elem, {
        accordion: false
    });

    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {});

    var instance = M.Tabs.init(document.getElementById("tabs"), {});
});

/**
 * currency formatter
 */
Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('en-GB', {
        style: 'currency',
        currency: 'GBP',
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});

Vue.filter('toHumanDate', function (value) {
    console.log('date', value)
    if(Vue.moment(value, "YYYY-MM-DD", true).isValid()) {
        return Vue.moment(value).format('DD-MM-YYYY');
    }
    if(Vue.moment(value, "YYYY-MM-DD HH:mm:ss", true).isValid()) {
        return Vue.moment(value).format('DD-MM-YYYY');
    }
    return value
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
