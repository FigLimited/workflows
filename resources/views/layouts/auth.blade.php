<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ config('app.name', 'Laravel') }}</title>

      <!-- Fonts -->
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    <div id="app">
      <nav class="brand-secondary">
        <div class="nav-wrapper">
          <a href="/" class="brand-logo right hide-on-small-only">
            <img src="/img/logo60h.png" alt="">
          </a>
          <a href="/" class="brand-logo-small right hide-on-med-and-up">
            <img src="/img/logo50h.png" alt="">
          </a>
        </div>
      </nav>

      <main class="py-4">
        @yield('content')
      </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

  </body>
</html>
