@extends('layouts.vue')

@section('content')
<jobs-show :job-id="{{ $id }}"></jobs-show>
@endsection
