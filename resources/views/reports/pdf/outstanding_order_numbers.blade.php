<html>
  <head>
    <style>
      @font-face {
        font-family: Raleway;
        src: url({{ url('fonts/Raleway-Regular.ttf')}});
      }
      @font-face {
        font-family: Raleway;
        src: url({{ url('fonts/Raleway-Light.ttf')}});
        font-weight: light;
      }
      @font-face {
        font-family: Raleway;
        src: url({{ url('fonts/Raleway-Bold.ttf')}});
        font-weight: bold;
      }

      #boundary {position:absolute;top:0;right:0;bottom:0;left:0;}
      #inner {position: absolute;left:0;top:0;right:0;bottom:0;}
      #body{margin-top:0mm;width:100%;}

      * {
        color: #333!important;
        font-family: 'Raleway'!important;
      }
      .pos-abs {
        position: absolute!important;
      }
      .big {
        font-size:24px;
        width:100%;
        line-height:24px;
      }
      .address {
        line-height: 1;
      }

      .page-break {
        page-break-after: always;
      }

      table thead tr th, table tbody tr td {
        padding: 0;
        margin: 0;
        line-height: 1;
      }
      table thead tr th {
        border-bottom: 2px solid #666;
      }
      table tbody tr td.description {
        border-bottom: 1px solid #999;
      }
    </style>
    <link href="{{ url('css/app.css') }}" rel="stylesheet" media="all">
  </head>
  <body>

    <div id="boundary">
      <div id="inner">
        <div id="body">

            <span class="right">as at {{ now() }}</span>
            <h4>
                Outstanding Order Numbers
            </h4>
            <table>
                <thead>
                    <tr>
                        <th>Job</th>
                        <th>Date Booked</th>
                        <th>Completed</th>
                        <th>Customer</th>
                        <th class="right-align">£ Nett</th>
                        <th class="right-align">£ Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $total = [];
                    @endphp
                    @foreach ($salesReps as $salesRep => $jobs)
                    <tr>
                        <td colspan="6"><h6>{{ $salesRep }}</h6></td>
                    </tr>
                        @foreach ($jobs as $job)
                        @php
                        if(isset($total[$salesRep])) {
                            $total[$salesRep] += $job->total;
                        } else {
                            $total[$salesRep] = $job->total;
                        }
                        @endphp
                        <tr>
                            <td>{{ $job->id }}</td>
                            <td>{{ $job->created_at }}</td>
                            <td>{{ $job->completed_at }}</td>
                            <td>{{ $job->customer->name }}</td>
                            <td class="right-align">@money($job->total)</td>
                            <td class="right-align">@money($job->gross)</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"></td>
                            <td class="right-align"><b>@money($total[$salesRep])</b></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="4" style="border-top: 1px solid #999;"><h6>Total</h6></td>
                        <td class="right-align" style="border-top: 1px solid #999;"><b>@money(array_sum($total))</b></td>
                        <td style="border-top: 1px solid #999;">&nbsp;</td>
                    </tr>
                </tbody>
            </table>

            <span class="right">printed by {{ $user->username }}</span>

        </div>
      </div>
    </div>

  </body>
</html>
