<ul class="sidenav sidenav-nav sidenav-fixed" id="mobile-demo">
    <li>
        <h6>General</h6>
    </li>
    <li>
        <div class="collection">
            <a href="{{ route('welcome') }}" class="collection-item brand-primary-text">Home</a>
        </div>
    </li>
    <li>
        <h6>Jobs</h6>
        <div class="collection">
            <a href="{{ route('jobs.dashboard') }}" class="collection-item {{ (Route::is('jobs.dashboard')) ? 'selected' : 'brand-secondary-text' }}">Dashboard</a>
            <a href="{{ route('jobs.search') }}" class="collection-item {{ (Route::is('jobs.search')) ? 'selected' : ' brand-secondary-text' }}">Search</a>
            @foreach($newProcesses as $process)
            <a href="{{ route('processes.new', ['process' => $process->id]) }}" class="collection-item brand-secondary-text">--> {{ $process->name }}</a>
            @endforeach
        </div>
    </li>
    <li>
        <h6>Reports</h6>
        <div class="collection">
            <a href="{{ route('reports.outstandingOrderNumbers') }}" class="collection-item {{ (Route::is('reports.outstandingOrderNumbers')) ? 'selected' : 'brand-secondary-text' }}">Outstanding Order Numbers</a>
            <a href="{{ route('reports.awaitingAcceptance') }}" class="collection-item {{ (Route::is('reports.awaitingAcceptance')) ? 'selected' : 'brand-secondary-text' }}">Awaiting Acceptance</a>
            <a href="{{ route('reports.byEngineer') }}" class="collection-item {{ (Route::is('reports.byEngineer')) ? 'selected' : 'brand-secondary-text' }}">by Engineer</a>
            <a href="{{ route('reports.awaitingCollection') }}" class="collection-item {{ (Route::is('reports.awaitingCollection')) ? 'selected' : 'brand-secondary-text' }}">Awaiting Collection</a>
            <a href="{{ route('reports.awaitingDelivery') }}" class="collection-item {{ (Route::is('reports.awaitingDelivery')) ? 'selected' : 'brand-secondary-text' }}">Awaiting Delivery</a>
            <a href="{{ route('reports.jobsOverdue') }}" class="collection-item {{ (Route::is('reports.jobsOverdue')) ? 'selected' : 'brand-secondary-text' }}">Overdue</a>
            <a href="{{ route('reports.monthlyOutput') }}" class="collection-item {{ (Route::is('reports.monthlyOutput')) ? 'selected' : 'brand-secondary-text' }}">Monthly Output</a>
        </div>
    </li>
    <li>
        <h6>Accounts</h6>
    </li>
    <li>
        <a href="{{ route('accounts.sageExport') }}" class="collection-item {{ (Route::is('accounts.sageExport')) ? '' : 'brand-secondary-text' }}">Sage Export</a>
        <a href="{{ route('accounts.sageExportReport') }}" class="collection-item {{ (Route::is('accounts.sageExportReport')) ? '' : 'brand-secondary-text' }}">Sage Export Report</a>
    </li>
</ul>
