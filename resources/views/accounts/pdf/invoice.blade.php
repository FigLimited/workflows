<html>
    <head>
        <style>

          @font-face {
            font-family: Raleway;
            src: url({{ url('fonts/Raleway-Regular.ttf')}});
          }
          @font-face {
            font-family: Raleway;
            src: url({{ url('fonts/Raleway-Light.ttf')}});
            font-weight: light;
          }
          @font-face {
            font-family: Raleway;
            src: url({{ url('fonts/Raleway-Bold.ttf')}});
            font-weight: bold;
          }
          * {
            color: #333!important;
            font-family: 'Raleway'!important;
          }
          #boundary {position:absolute;top:0;right:0;bottom:0;left:0;}
          #inner {position: absolute;left:0;top:0;right:0;bottom:0;}
          #body{margin-top:0mm;width:100%;}
          div {
            line-height: 1;
          }
          .bb {
            border-bottom: 1px solid #999;
          }
          .bt {
            border-top: 1px solid #999;
          }
          .pa-0 {
            padding: 0;
          }
          .qty {
            width: 20mm;
          }
          .money {
            width: 30mm;
          }
        </style>
        <link href="{{ url('css/app.css') }}" rel="stylesheet" media="all">
    </head>
    <body>

        <div id="boundary">
            <div id="inner">
                <div id="body">
                    
                    <div style="position:absolute;top:0;left:0;">{{ config('app.company') }}</div>
                    <div style="position:absolute;top:20px;left:0;">{{ config('app.address1') }}</div>
                    <div style="position:absolute;top:40px;left:0;">{{ config('app.address2') }}</div>
                    <div style="position:absolute;top:60px;left:0;">{{ config('app.address3') }}</div>
                    <div style="position:absolute;top:80px;left:0;">{{ config('app.postcode') }}</div>

                    <div style="position:absolute;top:0;right:0;"><img src="{{ public_path('img/logo70h.png') }}" style=""></div>

                    <div style="position:absolute;top:140px;left:0;">VAT Reg No: {{ config('app.vat_reg') }}</div>
                    <div style="position:absolute;top:140px;left:108mm;font-size:24px;font-weight:bold;">INVOICE</div>

                    <div style="position:absolute;top:200px;left:0;">{{ $job->customer->name }}</div>
                    <div style="position:absolute;top:220px;left:0;">
                      @if($job->customer->invoice_address !== null)
                        {!! nl2br($job->customer->invoice_address->address) !!}
                      @else
                        @nl2p(e($job->customer->delivery_address->address))
                      @endif
                    </div>

                    <!-- Document No -->
                    <div style="position:absolute;top:200px;left:108mm;font-weight:bold;">Document Number</div>
                    <div style="position:absolute;top:200px;right:0;" class="right-align">{{ $job->id }}</div>

                    <!-- Date -->
                    <div style="position:absolute;top:220px;left:108mm;font-weight:bold;">Date / Tax Point</div>
                    <div style="position:absolute;top:220px;right:0;" class="right-align">{{ $job->delivered_at }}</div>

                    <!-- Order -->
                    <div style="position:absolute;top:240px;left:108mm;font-weight:bold;">Order Number</div>
                    <div style="position:absolute;top:240px;right:0;" class="right-align">{{ $job->order_number }}</div>

                    <!-- Shipment Ref -->
                    <div style="position:absolute;top:260px;left:108mm;font-weight:bold;">Shipment Ref</div>
                    <div style="position:absolute;top:260px;right:0;" class="right-align">{{ $job->shipment_ref }}</div>

                    <!-- Account No -->
                    <div style="position:absolute;top:280px;left:108mm;font-weight:bold;">Account Number</div>
                    <div style="position:absolute;top:280px;right:0;" class="right-align">{{ $job->customer->code }}</div>
                    


                    <table style="position:absolute;top:340px;left:0;width:100%;">
                        <thead>
                            <tr>
                                <th class="bb pa-0 qty">Quantity</th>
                                <th class="bb pa-0 qty">Product</th>
                                <th class="bb pa-0">Details</th>
                                <th class="bb pa-0 right-align money">Unit Price</th>
                                <th class="bb pa-0 right-align money">Nett Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="pa-0">1</td>
                                <td class="pa-0">S3</td>
                                <td class="pa-0">{!! nl2br($job->note_sales->note) !!}</td>
                                <td class="pa-0 right-align">@money($job->nett)</td>
                                <td class="pa-0 right-align">@money($job->nett)</td>
                            </tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr><td class="pa-0" colspan="5">&nbsp;</td></tr>
                            <tr>
                                <td class="bt pa-0" colspan="3" rowspan="4" style="padding-top:6px;padding-right:6px;">All invoice queries must be brought the attention of the accounts department no later than 7 days after date of invoice. These should be submitted in writing to accounts@neutronictechnologies.com.</p><p>All goods and services are supplied to standard terms and conditions. The title to the goods remain the property of the vendor until full payment is received.</p><b>NETT 30 Days</b></td>
                                <td class="bt pa-0"><b>Nett</b></td>
                                <td class="bt pa-0 right-align">@money($job->nett)</td>
                            </tr>
                            <tr>
                                <td class="pa-0"><b>Carriage</b></td>
                                <td class="pa-0 right-align">@money($job->packaging + $job->delivery)</td>
                            </tr>
                            <tr>
                                <td class="pa-0"><b>VAT @ {{ $job->vat_rate }}%</b></td>
                                <td class="pa-0 right-align">@money($job->vat)</td>
                            </tr>
                            <tr>
                                <td class="pa-0"><b>Total</b></td>
                                <td class="pa-0 right-align">@money($job->gross)</td>
                            </tr>

                        </tbody>
                    </table>

                </div>
                
                
                
            </div>
        </div>
            
        

    </body>
</html>
