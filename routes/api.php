<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:api'])->name('api.')->namespace('Api')->group(function () {
    Route::get('jobs/dashboard', 'JobsController@dashboard')->name('jobs.dashboard');
    Route::post('jobs/search', 'JobsController@search')->name('jobs.search');
    Route::resource('jobs', 'JobsController');

    Route::get('customers/{customer}/contacts', 'CustomersController@contacts');
    Route::resource('customers', 'CustomersController');

    Route::resource('contacts', 'ContactsController');

    Route::get('suppliers/sub-contractors', 'SuppliersController@subContractors');
    Route::resource('suppliers', 'SuppliersController');

    Route::resource('priorities', 'PrioritiesController');

    Route::get('asset-manufacturers/{assetManufacturer}/asset-models', 'AssetManufacturersController@assetModels');
    Route::resource('asset-manufacturers', 'AssetManufacturersController');

    Route::resource('asset-models', 'AssetModelsController');

    Route::get('roles/is-job-type', 'RolesController@isJobType');
    Route::resource('roles', 'RolesController');

    Route::get('processes/new', 'ProcessesController@new')->name('processes.new');
    Route::get('processes/{process}/process/{job}', 'ProcessesController@process')->name('processes.process');
    Route::post('processes/{process}/process/{job}', 'ProcessesController@postProcess')->name('processes.postProcess');
    Route::post('processes/{process}/process', 'ProcessesController@postNew')->name('processes.postNew');
    Route::resource('processes', 'ProcessesController');

    Route::get('reports/outstanding-order-numbers/{pdf?}', 'ReportsController@outstandingOrderNumbers')->name('reports.outstandingOrderNumbers');
    Route::get('reports/awaiting-acceptance/{pdf?}/{mine?}', 'ReportsController@awaitingAcceptance')->name('reports.awaitingAcceptance');
    Route::get('reports/by-engineer/{pdf?}', 'ReportsController@byEngineer')->name('reports.byEngineer');
    Route::get('reports/awaiting-collection/{pdf?}', 'ReportsController@awaitingCollection')->name('reports.awaitingCollection');
    Route::get('reports/awaiting-delivery/{pdf?}', 'ReportsController@awaitingDelivery')->name('reports.awaitingDelivery');
    Route::get('reports/jobs-overdue/{pdf?}', 'ReportsController@jobsOverdue')->name('reports.jobsOverdue');
    Route::get('reports/monthly-output/{pdf?}/{month?}', 'ReportsController@monthlyOutput')->name('reports.monthlyOutput');

    Route::get('accounts/sage-export', 'AccountsController@sageExport')->name('accounts.sageExport');
    Route::post('accounts/{job}/send-invoice', 'AccountsController@sendInvoice')->name('accounts.sendInvoice');
    Route::get('accounts/{job}/view-invoice', 'AccountsController@viewInvoice')->name('accounts.viewInvoice');
    Route::post('accounts/export', 'AccountsController@export')->name('accounts.export');
    Route::post('accounts/jobs-delivered', 'AccountsController@jobsDelivered')->name('accounts.jobsDelivered');

    Route::get('/dev-email', 'AuthController@getDevEmail');
});

/**
 * unprotected
 */
Route::middleware(['api'])->name('api.')->namespace('Api')->group(function () {
    Route::post('login', 'AuthController@login');
});
