<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware('auth')->group(function() {
// });

Route::get('jobs/search', 'JobsController@search')->name('jobs.search');
Route::get('/jobs/dashboard', 'JobsController@dashboard')->name('jobs.dashboard');
Route::resource('jobs', 'JobsController');

Route::get('processes/{process}/process/{job}', 'ProcessesController@process')->name('processes.process');
// Route::post('processes/{process}/process/{job}', 'ProcessesController@postProcess')->name('processes.postProcess');
Route::get('processes/{process}/process', 'ProcessesController@new')->name('processes.new');
// Route::post('processes/{process}/process', 'ProcessesController@postNew')->name('processes.postNew');
Route::resource('processes', 'ProcessesController');

Route::get('reports/outstanding-order-numbers/{pdf?}', 'ReportsController@outstandingOrderNumbers')->name('reports.outstandingOrderNumbers');
Route::get('reports/awaiting-acceptance/{pdf?}/{mine?}', 'ReportsController@awaitingAcceptance')->name('reports.awaitingAcceptance');
Route::get('reports/by-engineer/{pdf?}', 'ReportsController@byEngineer')->name('reports.byEngineer');
Route::get('reports/awaiting-collection/{pdf?}', 'ReportsController@awaitingCollection')->name('reports.awaitingCollection');
Route::get('reports/awaiting-delivery/{pdf?}', 'ReportsController@awaitingDelivery')->name('reports.awaitingDelivery');
Route::get('reports/jobs-overdue/{pdf?}', 'ReportsController@jobsOverdue')->name('reports.jobsOverdue');
Route::get('reports/monthly-output/{pdf?}/{month?}', 'ReportsController@monthlyOutput')->name('reports.monthlyOutput');

Route::get('accounts/sage-export', 'AccountsController@sageExport')->name('accounts.sageExport');
Route::get('accounts/sage-export-report', 'AccountsController@sageExportReport')->name('accounts.sageExportReport');
Route::get('accounts/{job}/show-invoice', 'AccountsController@showInvoice')->name('accounts.showInvoice');
